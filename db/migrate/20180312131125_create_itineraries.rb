class CreateItineraries < ActiveRecord::Migration[5.0]
  def change
    create_table :itineraries do |t|
      t.string :origin
      t.string :destiny
      t.datetime :departure_time
      t.datetime :arrival_time
      t.string :ship
      t.integer :status

      t.timestamps
    end
  end
end
