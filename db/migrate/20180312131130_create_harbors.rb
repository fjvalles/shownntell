class CreateHarbors < ActiveRecord::Migration[5.0]
  def change
    create_table :harbors do |t|
      t.float :longitude
      t.float :latitude
      t.datetime :start_time
      t.datetime :end_time
      t.string :name
      t.string :commune
      t.integer :capacity

      t.timestamps
    end
  end
end
