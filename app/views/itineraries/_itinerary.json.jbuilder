json.extract! itinerary, :id, :origin, :destiny, :departure_time, :arrival_time, :ship, :status, :created_at, :updated_at
json.url itinerary_url(itinerary, format: :json)
