json.extract! harbor, :id, :longitude, :latitude, :start_time, :end_time, :name, :commune, :capacity, :created_at, :updated_at
json.url harbor_url(harbor, format: :json)
