require 'test_helper'

class HarborsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @harbor = harbors(:one)
  end

  test "should get index" do
    get harbors_url
    assert_response :success
  end

  test "should get new" do
    get new_harbor_url
    assert_response :success
  end

  test "should create harbor" do
    assert_difference('Harbor.count') do
      post harbors_url, params: { harbor: { capacity: @harbor.capacity, commune: @harbor.commune, end_time: @harbor.end_time, latitude: @harbor.latitude, longitude: @harbor.longitude, name: @harbor.name, start_time: @harbor.start_time } }
    end

    assert_redirected_to harbor_url(Harbor.last)
  end

  test "should show harbor" do
    get harbor_url(@harbor)
    assert_response :success
  end

  test "should get edit" do
    get edit_harbor_url(@harbor)
    assert_response :success
  end

  test "should update harbor" do
    patch harbor_url(@harbor), params: { harbor: { capacity: @harbor.capacity, commune: @harbor.commune, end_time: @harbor.end_time, latitude: @harbor.latitude, longitude: @harbor.longitude, name: @harbor.name, start_time: @harbor.start_time } }
    assert_redirected_to harbor_url(@harbor)
  end

  test "should destroy harbor" do
    assert_difference('Harbor.count', -1) do
      delete harbor_url(@harbor)
    end

    assert_redirected_to harbors_url
  end
end
